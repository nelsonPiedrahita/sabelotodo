<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_model');
		$this->load->helper('url');
		$this->load->library('calendar');

		header("cache-Control: no-store, no-cache, must-revalidate");
		header("cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
	}

	public function index()
	{
		$this->load->helper('form');
		
		$data['main_title'] = 'Sabelotodo';
		$data['title2'] = 'Home';
		$this->load->view('templates/header', $data);
		$this->load->view('sabelotodo/home_view', $data);
		$this->load->view('templates/footer');

	}

	

}
?>