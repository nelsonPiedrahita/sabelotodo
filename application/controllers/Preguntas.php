<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Preguntas extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();
			$this->load->model('preguntas_model');
			$this->load->helper('url');
			$this->load->library('calendar');

		}

		public function index()
		{
			$data['main_title'] = 'Sabelotodo';

			$this->load->helper('form');
			$this->load->library('form_validation');

			$this->form_validation->set_rules('usuario', 'Usuario', 'required');
			if ($this->form_validation->run() === FALSE)
			{
				$data['title2'] = 'Home';
				$this->load->view('templates/header', $data);
				$this->load->view('sabelotodo/home_view');
				$this->load->view('templates/footer');
			}
			else
			{
				
				
				$data['title2'] = 'Preguntas';
				$data['usuarioID'] = $this->input->post('usuarioID');
				$data['usuario'] = $this->input->post('usuario');
				$data['radio'] = $this->input->post('radio');

				if($data['radio'] == "")
				{
					$data['puntajeAcum'] = $this->input->post('puntaje');
					$data['usuarioID'] = $this->preguntas_model->add();	
				}
				else
				{	
					$this->preguntas_model->add_respuesta();
					if($data['radio'] == 1)
					{
						$data['puntajeAcum'] = $this->input->post('acumulado') + $this->input->post('puntaje');
					}
					else
					{
						$data['puntajeAcum'] = $this->input->post('acumulado');
					}
				}

				$resultado = $this->preguntas_model->get_preguntascontestadas($data['usuarioID']);

				$this->load->view('templates/header_preguntas', $data);
				if(sizeof($resultado) < 10) 
				{
					if(count($resultado) > 0)
					{
						$idPregunta = $this->excluir($resultado);
					} 
					else
					{
						$idPregunta = mt_rand(1,10);
					}

					$data['datos'] = $this->preguntas_model->get_pregunta($idPregunta); 

					$this->load->view('sabelotodo/preguntas_view.php', $data);
				}
				else 
				{
					echo "FINALIZADO";
					echo $data['puntajeAcum']/10;
				}
				$this->load->view('templates/footer_preguntas');

			}

		}

		public function excluir($arrExcluir)
		{	

			$aleatorio = mt_rand(1,10);
			$key = array_search($aleatorio, array_column($arrExcluir, 'preguntaID'));

			while (false !== $key){
				$aleatorio = mt_rand(1,10);
				$key = array_search($aleatorio, array_column($arrExcluir, 'preguntaID'));
			}

			return $aleatorio;

		}

	}
?>