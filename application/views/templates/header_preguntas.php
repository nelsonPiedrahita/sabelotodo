<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!doctype html>
<html lang="es">
    <head>
        <title><?php echo $main_title;?></title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <!-- Custom-Stylesheet-Links -->
        <!-- Bootstrap-CSS -->    
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css"  type="text/css" media="all">
        <!-- Index-Page-CSS -->   
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css"          type="text/css" media="all">
        <!-- Owl-Carousel-CSS --> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl.carousel.css"   type="text/css" media="all">
        <!-- Chocolat-CSS -->     
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/chocolat.css"       type="text/css" media="all">
        <!-- Animate-CSS -->      
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/animate-custom.css" type="text/css" media="all">
        <!-- //Custom-Stylesheet-Links -->

        <!-- Fonts -->
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat:400,700"     type="text/css" media="all">
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,100,300,500" type="text/css" media="all">
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Press+Start+2P"         type="text/css" media="all">
        <!-- //Fonts -->

        <!-- Font-Awesome-File-Links -->
        <!-- CSS --> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/font-awesome.min.css"      type="text/css" media="all">
        <!-- TTF --> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>fonts/fontawesome-webfont.ttf" type="text/css" media="all">
        <!-- //Font-Awesome-File-Links -->


    </head>
    <body>

        <!-- Header -->
        <div class="agileheader" id="agileitshome">

            <!-- Navigation -->
            <div class="w3lsnavigation">
                <nav class="navbar navbar-inverse agilehover-effect wthreeeffect navbar-default">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Logo -->
                        <div class="logo">
                            <a class="navbar-brand logo-w3l button" href=""><?php echo $main_title;?></a>
                        </div>
                        <!-- //Logo -->
                    </div>

                    <div id="navbar" class="navbar-collapse navbar-right collapse">
                        <ul class="nav navbar-nav navbar-right cross-effect" id="cross-effect">
                            <li><a class="scroll" href="#play">SALIR</a></li>
                        </ul>
                    </div><!-- //Navbar-Collapse -->

                </nav>
            </div>
            <!-- //Navigation -->

        </div>
        <!-- //Header -->