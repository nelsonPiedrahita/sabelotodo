
select t2.preguntaID 
from contestando t1 
inner join preguntas_contestando t2 on t1.contestandoID = t2.contestandoID 
where t1.contestandoID = 1;


SELECT pregunta 
FROM preguntas 
WHERE preguntaID = 1

SELECT * FROM preguntas t1 INNER JOIN respuestas t2 on t1.preguntaID = t2.preguntaID WHERE t1.preguntaID = 1

-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Respuestas;
DROP TABLE IF EXISTS Preguntas_Contestando;
DROP TABLE IF EXISTS Contestando;
DROP TABLE IF EXISTS Preguntas;

CREATE TABLE Preguntas (
    preguntaID int NOT NULL auto_increment,
    pregunta varchar(100),
    dificultad int,
    puntaje int,
    PRIMARY KEY (preguntaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE Respuestas (
    respuestaID int NOT NULL auto_increment,
    preguntaID int NOT NULL,
    respuesta varchar(100),
    correcta boolean,
    PRIMARY KEY (respuestaID),
    FOREIGN KEY (preguntaID) REFERENCES Preguntas(preguntaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE Contestando (
    contestandoID int NOT NULL auto_increment,
    usuario varchar(100),
    puntaje_prom double,
    PRIMARY KEY (contestandoID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE Preguntas_Contestando (
    preguntacontestadoID int NOT NULL auto_increment,
    preguntaID int NOT NULL,
    contestandoID int NOT NULL,
    correcta boolean,
    puntaje_acum int,
    contestada boolean,
    PRIMARY KEY (preguntacontestadoID),
    CONSTRAINT fk_pID FOREIGN KEY (preguntaID) REFERENCES Preguntas(preguntaID),
    CONSTRAINT fk_cID FOREIGN KEY (contestandoID) REFERENCES Contestando(contestandoID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (1, '¿Cómo se llama el estrecho que separa a España de Marruecos?', 5, 45);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(1, 1, 'El estrecho de Bering', False),
	(2, 1, 'El estrecho de Torres', False),
	(3, 1, 'El estrecho de Torres', True)
;

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (2, '¿Cuántas tribus se considera que existen en Kenia?', 9, 83);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(4, 2, '70', True),
	(5, 2, '30', False),
	(6, 2, '50', False)
;

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (3, '¿Qué pieza arqueológica fue hallada en Pasca?', 10, 95);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(7, 3, 'El poporo Quimbaya', False),
	(8, 3, 'La balsa Muisca', True),
	(9, 3, 'El Dorado', False)
;

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (4, 'En el Amazonas colombiano ¿Qué es una Maloca?', 9, 81);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(10, 4, 'Un museo', False),
	(11, 4, 'Un cementerio', False),
	(12, 4, 'Un lugar de reunión', True)
;

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (5, '¿Cuál es la moneda de Costa Rica?', 4, 35);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(13, 5, 'Cólon costarricense', True),
	(14, 5, 'Dolar', False),
	(15, 5, 'Peso costarricense', False)
;

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (6, '¿Qué es común entrelas ciudades de Venecia y Bangkok?', 6, 54);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(16, 6, 'Son igual de antiguas', False),
	(17, 6, 'Son igual de grandes', False),
	(18, 6, 'Se están hundiendo', True)
;

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (7, '¿A cuál colonia pertenecía Singapur en el siglo XIX?', 7, 69);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(19, 7, 'Holandesa', False),
	(20, 7, 'Británica', True),
	(21, 7, 'Francesa', False)
;

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (8, '¿Qué tienen de especial muchos de los pájaros de Nueva Zelanda?', 5, 41);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(22, 8, 'No tienen pico', False),
	(23, 8, 'No pueden volar', True),
	(24, 8, 'Comen carne', False)
;

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (9, 'Samoa está dividido en dos, ¿Qué pais maneja la otra parte?', 5, 47);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(25, 9, 'Estados Unidos', True),
	(26, 9, 'Japón', False),
	(27, 9, 'Australia', False)
;

INSERT INTO Preguntas (preguntaID, pregunta, dificultad, puntaje)
VALUES (10, '¿Capital de Dinamarca?', 2, 15);

INSERT INTO Respuestas (respuestaID, preguntaID, respuesta, correcta)
VALUES 
	(28, 10, 'Paris', False),
	(29, 10, 'Madrid', False),
	(30, 10, 'Copenhague', True)
;