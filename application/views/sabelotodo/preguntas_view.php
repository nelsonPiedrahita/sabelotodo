<!-- About -->
<div class="w3lsaboutaits" id="play">
	<div class="container">
		<div class="w3lsaboutaits-grids">
			<div class="col-md-12 w3lsaboutaits-grid w3lsaboutaits-grid-1">
				<div class="heading">
<?php 

	defined('BASEPATH') OR exit('No direct script access allowed');

	echo validation_errors();

	$hidden = array(
		'usuarioID' 		=> $usuarioID, 
		'usuario'    		=> $usuario,
		'preguntaID'		=> $datos[0]['preguntaID'],
		'puntaje'			=> $datos[0]['puntaje'],
		'acumulado'			=> $puntajeAcum
	);

	echo form_open('preguntas','',$hidden);

	echo '<h2>';
	echo '(' . $datos[0]['puntaje'] .') '.  $datos[0]['pregunta']; 
	echo '</h2>';

	$i = 1;
	foreach ($datos as $key)
	{
		$bool = FALSE;
		if ($i == 1)
		{
			$bool = TRUE;
		}
		echo '<h3>';
		echo form_radio('radio', $key['correcta'], $bool);
		echo ( $i . ') ' . $key['respuesta']);
		echo '</h3>';
		$i++;
	}
	echo "<div class='agilecontactw3ls'>";
	echo form_submit('submit', 'Siguiente pregunta');
	echo "</div>";
	echo form_close();

?>
</div>
</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!-- //About -->