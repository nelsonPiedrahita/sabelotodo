<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


	<!-- Footer -->
	<div class="agilefooterwthree" id="agilefooterwthree">
		<div class="container">

			<div class="agilefooterwthreebottom">
				<div class="col-md-6 agilefooterwthreebottom-grid agilefooterwthreebottom-grid1">
					<div class="copyright">
						<p>© 2017 Game Robo. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="=_blank"> W3layouts </a></p>
					</div>
				</div>
				<div class="col-md-6 agilefooterwthreebottom-grid agilefooterwthreebottom-grid2">
				</div>
			</div>

		</div>

		<a href="#agileitshome" class="agileto-top scroll" title="To Top"><img src="<?php echo base_url(); ?>images/to-top.png" alt="Game Robo"></a>

	</div>
	<!-- //Footer -->

		<!-- Custom-JavaScript-File-Links -->

		<!-- Default-JavaScript -->   
		<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.1.4.min.js"></script>
		<!-- Bootstrap-JavaScript --> 
		<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

		<!-- Resopnsive-Slider-JavaScript -->
			<script src="<?php echo base_url(); ?>js/responsiveslides.min.js"></script>
			<script>
				$(function () {
					$("#slider").responsiveSlides({
						auto: true,
						nav: true,
						speed: 2000,
						namespace: "callbacks",
						pager: true,
					});
				});
			</script>
		<!-- //Resopnsive-Slider-JavaScript -->

		<!-- Tab-JavaScript -->
			<script src="<?php echo base_url(); ?>js/cbpFWTabs.js"></script>
			<script>
				(function() {
					[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
						new CBPFWTabs( el );
					});
				})();
			</script>
		<!-- //Tab-JavaScript -->

		<!-- Owl-Carousel-JavaScript -->
			<script src="<?php echo base_url(); ?>js/owl.carousel.js"></script>
			<script>
				$(document).ready(function() {
					$("#owl-demo, #owl-demo1, #owl-demo2, #owl-demo3, #owl-demo4").owlCarousel({
						autoPlay: 3000,
						items : 5,
						itemsDesktop : [1024,4],
						itemsDesktopSmall : [414,3]
					});
				});
			</script>
		<!-- //Owl-Carousel-JavaScript -->

		<!-- Stats-Number-Scroller-Animation-JavaScript -->
			<script src="<?php echo base_url(); ?>js/waypoints.min.js"></script> 
			<script src="<?php echo base_url(); ?>js/counterup.min.js"></script> 
			<script>
				jQuery(document).ready(function( $ ) {
					$('.counter').counterUp({
						delay: 10,
						time: 1000
					});
				});
			</script>
		<!-- //Stats-Number-Scroller-Animation-JavaScript -->

		<!-- Popup-Box-JavaScript -->
			<script src="<?php echo base_url(); ?>js/jquery.chocolat.js"></script>
			<script type="text/javascript">
				$(function() {
					$('.w3portfolioaits-item a').Chocolat();
				});
			</script>
		<!-- //Popup-Box-JavaScript -->

		<!-- Smooth-Scrolling-JavaScript -->
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					$(".scroll").click(function(event){
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
					});
				});
			</script>
		<!-- //Smooth-Scrolling-JavaScript -->

	<!-- //Custom-JavaScript-File-Links -->



</body>
<!-- //Body -->



</html>